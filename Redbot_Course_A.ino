#include <NewPing.h>

int   max_distance  = 50; // maximum distance (cm) to ping for. Values over this read as zero.
int   U_send_pin    = 8;  // upper sensor trigger pin     purp
int   U_read_pin    = 9;  // upper sensor echo pin        gray
int   D_send_pin    = 10; // lower sensor trigger pin     yellow
int   D_read_pin    = 11; // lower sensor echo pin        green

NewPing U_sonar(U_send_pin,U_read_pin,max_distance);
NewPing D_sonar(D_send_pin,D_read_pin,max_distance);

int   L_motor_pin   = 3;
int   R_motor_pin   = 6;

int   range         = 25;    // any distance under this (cm) is considered a wall.
int   U_prox        = 0;     // proximity (cm) of object in front of upper proximity sensor
int   D_prox        = 0;     // proximity (cm) of object in front of lower proximity sensor
bool  U_wall        = false; // sets to true if upper proximity is under the range
bool  D_wall        = false; // sets to true if lower proximity is under the range

int   mode                  = 0;     // determines the action to perform via switch/case
long  last_action_end_time  = 0;     // time when the last action was completed

// NOTE: all time is in ms

// in orientation mode, if the device is facing a wall, turn right for this much time before going back to orienting itself
int orientation_mode_wall_turn = 10000;

// the speed for each "step" that the device takes:
int step_speed = 500;

// time in ms required to turn the device some unknown degrees left or right
int turn_time = 300;

// ===========================================================================================
// Setup
// ===========================================================================================

void setup(){
  Serial.begin(9600);
  pinMode(R_motor_pin,OUTPUT);
  pinMode(L_motor_pin,OUTPUT);
  }

// ===========================================================================================
// Loop
// ===========================================================================================

void loop(){

  // Orientation Mode:
  if(mode == 0){
    proc_sensors();
    if(D_wall == true){
      delay(50);
      digitalWrite(R_motor_pin,HIGH);
      delay(orientation_mode_wall_turn);
      digitalWrite(R_motor_pin,LOW);
      mode = 9;
      }
    else{mode = 9;}
    }
    
  else if(mode == 9){
    delay(5);
    digitalWrite(R_motor_pin,HIGH);
    delay(step_speed);
    digitalWrite(R_motor_pin,LOW);

    proc_sensors();

    if(D_wall == true){
      delay(1000);
      digitalWrite(L_motor_pin,HIGH);
      delay(1100);
      digitalWrite(L_motor_pin,LOW);
      mode = 1;
      }
    }

  // Wait for the last action to complete
  else if (millis() >= last_action_end_time){
    
    Serial.println("------------------------------------------------------------------");
    Serial.println("Whew! Just finished my last action. I wonder what's next...");

    // Stop Motors
    // =============================
    digitalWrite(R_motor_pin,LOW);
    digitalWrite(L_motor_pin,LOW);    
    delay(step_speed);

    // Check Proximity
    proc_sensors();
    
    // Determine Mode
    // ============================
    if      (U_wall == false && D_wall == false) { mode = 1; }
    else if (U_wall == true  && D_wall == false) { mode = 2; }
    else if (U_wall == false && D_wall == true ) { mode = 3; }
    else                                         { mode = 4; }
  
    // Set Next Action  
    // ============================
    switch (mode){

      // Forward
      // --------------------------
      case 1:
        digitalWrite(L_motor_pin,HIGH);
        digitalWrite(R_motor_pin,HIGH);
        last_action_end_time = millis() + turn_time;
        Serial.println("Case 1: Time to Move!");
        
        break;
        
      // Left
      // --------------------------
      case 2:
        digitalWrite(L_motor_pin,LOW);
        digitalWrite(R_motor_pin,LOW);
        last_action_end_time = millis() + 99999999999999999999999999999999; // this high move time is just so the device stops. There's better ways to do this, this is just temporary.
        Serial.println("Case 4: Stop!!!");
        break;

      // Right
      // --------------------------
      case 3:
        digitalWrite(L_motor_pin,LOW);
        digitalWrite(R_motor_pin,LOW);
        last_action_end_time = millis() + 99999999999999999999999999999999; // this high move time is just so the device stops. There's better ways to do this, this is just temporary.
        Serial.println("Case 4: Stop!!!");
        break;
        
      // Stop
      // --------------------------
      case 4:
        digitalWrite(L_motor_pin,LOW);
        digitalWrite(R_motor_pin,LOW);
        last_action_end_time = millis() + 99999999999999999999999999999999; // this high move time is just so the device stops. There's better ways to do this, this is just temporary.
        Serial.println("Case 4: Stop!!!");
        break;
        
      } // end switch
    } // end conditional  
    
  } // end loop

// Function: Check Proximity
void proc_sensors(){
  delay(50);
  U_prox = D_sonar.ping() / US_ROUNDTRIP_CM;
  delay(50);
  D_prox = U_sonar.ping() / US_ROUNDTRIP_CM;
  U_wall = (0 < U_prox && U_prox < range) ? true : false ;
  D_wall = (0 < D_prox && D_prox < range) ? true : false ;
  Serial.println("Upper Prox: " + String(U_prox) + " | Lower Prox: " + String(D_prox));
  Serial.println("Upper Wall: " + String(U_wall) + " | Lower Wall: " + String(D_wall));
  }
