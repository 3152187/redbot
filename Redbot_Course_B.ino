#include <NewPing.h>

// =======================================================================================================
// Processing Variables
// =======================================================================================================

int     L_motor_pin     = 3;        // pin: left motor
int     R_motor_pin     = 6;        // pin: right motor
int     U_send_pin      = 8;        // pin: upper sensor trigger (purple wire)
int     U_read_pin      = 9;        // pin: upper sensor echo (gray wire)
int     D_send_pin      = 10;       // pin: lower sensor trigger (yellow wire)
int     D_read_pin      = 11;       // pin: lower sensor echo (green wire)
int     max_distance    = 50;       // maximum distance (cm) to ping for. Values over this read as zero.
int     U_prox          = 0;        // proximity (cm) of object in front of upper proximity sensor
int     D_prox          = 0;        // proximity (cm) of object in front of lower proximity sensor
bool    U_wall          = false;    // sets to true if upper proximity is under the range
bool    D_wall          = false;    // sets to true if lower proximity is under the range
int     mode            = 0;        // determines the next action to perform. Starts at 0.

// Different Modes:
// 0 = Device is turned on (default state)
// 1 = Move Forward
// 2 = Turn Left
// 3 = Turn Right
// 4 = Stop
// 9 = Orientation Mode

NewPing U_sonar(U_send_pin,U_read_pin,max_distance);
NewPing D_sonar(D_send_pin,D_read_pin,max_distance);

long  last_action_end_time = 0;     // time when the last action was completed

// =======================================================================================================
// Calibration Variables
// =======================================================================================================

// Any distance (cm) under this is considered a wall.
int range = 25;

// The amount of time to wait in between each 'step' or 'turn'.
int step_delay = 50;

// For Orientation Mode: If the device starts facing a wall, turn right for this much time before returning to regular orientation behavior
int orientation_mode_wall_avoid = 1500;

// For Orientation Mode: If the device (in regular orientation mode) finds a wall, turn left for this much time to align with the path
int orientation_mode_wall_align = 1000;

// For Normal Mode: time required to turn the device some unknown degrees left or right
int turn_time = 21600;

// 1800 x 12 to turn time

// For Normal Mode: how long you want the device to move forward during each step
int forward_time = 100;

// =======================================================================================================
// Setup
// =======================================================================================================

void setup(){
    Serial.begin(9600);
    pinMode(R_motor_pin,OUTPUT);
    pinMode(L_motor_pin,OUTPUT);
    }

// =======================================================================================================
// Loop
// =======================================================================================================

void loop(){

    // Wait for last action to complete
    if (millis() >= last_action_end_time){

        Serial.println("------------------------------------------------------------------");

        // Stop Motors
        digitalWrite(R_motor_pin,LOW);
        digitalWrite(L_motor_pin,LOW);
        delay(step_delay);

        // Check Proximity
        proc_sensors();

        // Determine next action to take
        if      (U_wall == false && D_wall == false) { mode = 1; }
        else if (U_wall == true  && D_wall == false) { mode = 2; }
        else if (U_wall == false && D_wall == true ) { mode = 3; }
        else                                         { mode = 4; }
        
        switch (mode){
            
          // Forward
          // --------------------------
          case 1:
            digitalWrite(L_motor_pin,HIGH);
            digitalWrite(R_motor_pin,HIGH);
            last_action_end_time = millis() + forward_time;
            Serial.println("Case 1: Time to Move!");

            break;

          // Left
          // --------------------------
          case 2:
            digitalWrite(R_motor_pin,HIGH);
            last_action_end_time = millis() + turn_time;
            Serial.println("Case 2: Turn Left!");
            break;

          // Right
          // --------------------------
          case 3:
            digitalWrite(L_motor_pin,HIGH);
            last_action_end_time = millis() + turn_time;
            Serial.println("Case 3: Turn Right!");
            break;

          // Stop
          // --------------------------
          case 4:
            digitalWrite(L_motor_pin,LOW);
            digitalWrite(R_motor_pin,LOW);
            Serial.println("Case 4: Stop!!!");
            break;

            } // end switch
        } // end conditional
    } // end loop

// =======================================================================================================
// Accessory Functions
// =======================================================================================================

// Get proximity sensor data;
void proc_sensors(){
    delay(50);
    U_prox = D_sonar.ping() / US_ROUNDTRIP_CM;
    delay(50);
    D_prox = U_sonar.ping() / US_ROUNDTRIP_CM;
    U_wall = (0 < U_prox && U_prox < range) ? true : false ;
    D_wall = (0 < D_prox && D_prox < range) ? true : false ;
    Serial.println("Upper Prox: " + String(U_prox) + " | Lower Prox: " + String(D_prox));
    Serial.println("Upper Wall: " + String(U_wall) + " | Lower Wall: " + String(D_wall));
    }
